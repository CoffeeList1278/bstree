﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tree
{
    class BSTree
    {
        private Node _root;

        public void Insert(int value)
        {
            if (value < _root.Value)
            {
                _insert(value, _root.LeftChild);
            }
            if (value > _root.Value)
            {
                _insert(value, _root.RightChild);
            }
        }

        private void _insert(int value, Node parent)
        {
            if (parent == null)
            {
                Node parent = new Node();
                parent.Value = value;
            }
            if (value < parent.Value)
            {
                _insert(value, parent.LeftChild);
            }
            if (value > parent.Value)
            {
                _insert(value, parent.RightChild);
            }

        }

        public Node FindValue(BSTree tree, int value)
        {
            if (value==tree._root.Value)
            {
                return (tree._root);
            }
            else if(value < tree._root.Value)
            {
                return _findValue(tree._root.LeftChild,value);
            }
            else
            {
                return (_findValue(tree._root.RightChild,value));
            }
        }

        private Node _findValue(Node node, int value)
        {
            if (node == null) return null;
            if (value==node.Value)
            {
                return (node);
            }
            else if(value < node.Value)
            {
                return (_findValue(node.LeftChild,value));
            }
            else
            {
                return (_findValue(node.RightChild,value));
            }
        }

        public void Delete(int value)
        {
            Node current = _root;
            Node change = new Node();
            Node replace = new Node();
            int temp = 0;


            while (true)
            {
                if (current.Value == value)
                {
                    if (current.RightChild!=null & current.LeftChild!=null)
                    {
                        temp = _findMinimum(current.RightChild);
                        Delete(_findMinimum(current.RightChild));
                        current.Value = temp;
                        break;
                    }
                    else if(current.RightChild==null & current.LeftChild!=null)
                    {
                        replace = current.LeftChild;
                        current = replace;
                        break;
                    }
                    else
                    {
                        replace = current.RightChild;
                        current = replace;
                        break;
                    }

                }
                else if (value > current.Value)
                {
                    if (current.RightChild != null)
                    {
                        change = current.RightChild;
                    }
                    else { break; }
                }
                else
                {
                    if (current.LeftChild != null)
                    {
                        change = current.LeftChild;
                    }
                    else { break; }
                }
            }
        }

        public bool Search(int value)
        {
            if (value < _root.Value)
            {
                return (_search(value, _root.LeftChild));
            }
            if (value > _root.Value)
            {
                return (_search(value, _root.RightChild));
            }
            else { return true; }
        }

        private bool _search(int value,Node parent)
        {
            if (parent == null) { return false; }
            else if (value < parent.Value)
            {
                return (_search(value, parent.LeftChild));
            }
            if (value > parent.Value)
            {
                return (_search(value, parent.RightChild));
            }
            else { return true; }

        }

        public int FindMinimum()
        {
            if (_root == null) { return 0; }
            else if (_root.LeftChild == null) { return _root.Value; }
            else { return (_findMinimum(_root.LeftChild)); }
        }

        private int _findMinimum(Node node)
        {
            if (node.LeftChild == null) { return node.Value; }
            else { return (_findMinimum(node.LeftChild)); }
        }
    }
}
